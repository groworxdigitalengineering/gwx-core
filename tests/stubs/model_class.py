from gwx_core.services.abstract_serializer_service import AbstractSerializerService


class ModelClass(AbstractSerializerService):
    __serializable__ = ['sample']

    sample: str = None

    def __init__(self):
        super().__init__()
        self.sample = 'sample'
