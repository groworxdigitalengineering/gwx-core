from tests.stubs.empty_class import EmptyClass
from tests.stubs.model_class import ModelClass
from tests.utils_test_case import UtilsTestCase


class AbstractSerializerServiceTest(UtilsTestCase):

    def test_to_json_raise_attribute_error(self):
        """Assert that the AbstractSerializerService.to_json method raises an AttributeError,
        when an empty class implementing it was invoke.

        :return: None
        """

        self.assertRaiseWithMessageOnResponse(
            AttributeError,
            EmptyClass.to_json,
            "'function' object has no attribute '__serializable__'",
            'Serializable attribute is None, serializable fields must be defined.',
            None,
            None,
            False
        )

    def test_to_json_successfully(self):
        """Assert that the AbstractSerializerService.to_json method,
        returns a serialized format of the hydrated model.

        :return: None
        """

        self.assertEqual({'sample': 'sample'}, ModelClass().to_json())
